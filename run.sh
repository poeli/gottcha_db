#!/bin/bash
PATH=scripts:$PATH
PREFIX=gottcha2_refseq89
LISTDIR=gottcha2_listdir
OUTDIR=gottcha2_outdir

mkdir -p $LISTDIR
mkdir -p $OUTDIR

rm -rf gottcha2_refseq89.custom.db

gottcha_db_prep.py \
  -sdb gottcha2_refseq89.custom.db \
  -asr assembly_summary_refseq_plus_Humans.txt \
  -asd Refseq_genomes \
  -sqt 9606 \
  -scf \
  -p $LISTDIR/$PREFIX &> $PREFIX.log

JID=`qsub ../scripts/uge_gottcha_db.sh $LISTDIR $OUTDIR $PREFIX`
echo $JID
JID=`echo $JID | awk 'match($0,/[0-9]+/){print substr($0, RSTART, RLENGTH)}'`

for RANK in strain species genus family order class phylum superkingdom;
do
    LIST="$LISTDIR/$PREFIX.$RANK.list"
    
    ASR=assembly_summary/ARCH-BACT-assembly_summary.txt
    PREFIX=RefSeq-Release89.Bacteria

    qsubbin -pe smp 2 -l h_vmem=50G -hold_jid $JID -N $PREFIX-postprocess-$RANK "gottcha_db_postprocess.py -asr $ASR -l $LIST -gdb $OUTDIR -r $RANK -p $DBDIR/$PREFIX"
    
    ASR=assembly_summary/viral-assembly_summary.txt
    PREFIX=RefSeq-Release89.Virus
    
    qsubbin -pe smp 2 -l h_vmem=50G -hold_jid $JID -N $PREFIX-postprocess-$RANK "gottcha_db_postprocess.py -asr $ASR -l $LIST -gdb $OUTDIR -r $RANK -p $DBDIR/$PREFIX"
    
    ASR=assembly_summary/plant-assembly_summary.txt
    PREFIX=RefSeq-Release89.Plant
    
    qsubbin -pe smp 2 -l h_vmem=50G -hold_jid $JID -N $PREFIX-postprocess-$RANK "gottcha_db_postprocess.py -asr $ASR -l $LIST -gdb $OUTDIR -r $RANK -p $DBDIR/$PREFIX"
    
    ASR=assembly_summary/fungi-assembly_summary.txt
    PREFIX=RefSeq-Release89.Fungi
    
    qsubbin -pe smp 2 -l h_vmem=50G -hold_jid $JID -N $PREFIX-postprocess-$RANK "gottcha_db_postprocess.py -asr $ASR -l $LIST -gdb $OUTDIR -r $RANK -p $DBDIR/$PREFIX"
    
    ASR=assembly_summary/protozoa-assembly_summary.txt
    PREFIX=RefSeq-Release89.Protozoa

    qsubbin -pe smp 2 -l h_vmem=50G -hold_jid $JID -N $PREFIX-postprocess-$RANK "gottcha_db_postprocess.py -asr $ASR -l $LIST -gdb $OUTDIR -r $RANK -p $DBDIR/$PREFIX"
done

parallel 'qsub -S /bin/sh -N {}.stats -V -cwd -b y -j y -m abe -M po-e@lanl.gov -pe smp 2 -l h_vmem=50G "gottcha_ann2stats.py {} > {.}.stats"' ::: *.ann
